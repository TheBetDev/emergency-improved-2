@echo off
title Emergency Improved 2.2 Installer (STEAM copy)
color f0
echo ============================
echo EMERGENCY IMPROVED 2.2
echo INSTALLER
echo ============================
timeout 3
echo Checking if you have the game in your steamapps folder..
IF EXIST "C:\Program Files (x86)\Steam\steamapps\common\Grand Theft Auto V" (
	echo Game found! Now looking for Menyoo installation
	IF EXIST "C:\Program Files (x86)\Steam\steamapps\common\Grand Theft Auto V\Menyoo.asi" (
		echo Menyoo.asi found! Now looking for menyooStuff folder...
			IF EXIST "C:\Program Files (x86)\Steam\steamapps\common\Grand Theft Auto V\menyooStuff\Spooner" (
				echo Found menyooStuff folder! Inside was also Spooner folder. But first, we are going to check if you have the map already installed.
				IF EXIST "C:\Program Files (x86)\Steam\steamapps\common\Grand Theft Auto V\menyooStuff\Spooner\Emergency Improved 2.2.xml" (
					echo You have the map already installed! You don't have to waste time opening the installer again!
				) ELSE (
					echo Map is not installed. Installing in 5 seconds.
					timeout 5
					COPY "Emergency Improved 2.2.xml" "C:\Program Files (x86)\Steam\steamapps\common\Grand Theft Auto V\menyooStuff\Spooner"
					IF EXIST "C:\Program Files (x86)\Steam\steamapps\common\Grand Theft Auto V\menyooStuff\Spooner\Emergency Improved 2.2.xml" (
						echo Succesfully installed! You are good to go!
					) ELSE (
						echo Installer sent the command to copy the file to destination, but after this there was no file there.
						echo ERROR. Please see error above.
					)
				)
			) ELSE (
				echo Oops! menyooStuff is not there... Please, move menyooStuff folder from the ZIP where Menyoo.asi was and try again.
			)
	) ELSE (
		echo Menyoo.asi not found... You need to install Menyoo. If you have the game via Steam but in another location, use the Manual installation. My apologies.
	)
) ELSE (
	echo You don't have the game via Steam or you don't have it installed in the default location. Please use Manual installation.
)
pause
